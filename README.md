# Rim Wildlife Uplifted

Rim Wildlife Uplifted is a WIP mod designed to replace all wildlife in Rimworld with beast-featured wildmen. A different angle on furrifying your Rimworld.

How RimWU works:
- RimWU adds new species and pawns to the game and patches the vanilla biomes to replace spawning rates for their vanilla counterparts with the animal-person wildmen. This means that any other mod's patch for those animals will still technically work, because, for instance, the boomalope def still exists, but it won't apply to the animal people. This also means that any modded biomes will need patches.
- For insectoids, RimWU patches the "hive" building to replace the marked spawns with the appropriate insectoid person.
- RimWU adds trade tags to the animal people to make them spawn as animal trader merchandise (needs to be tested if it actually works) and patches the trade tags out of the vanilla counterparts to make them invalid.
- RimWU patches the vanilla humanoid think tree (order of operations for what a pawn does when not explicitly ordered to do something) to add an egg-laying behavior to all humanlike pawns who can do so, as well as think tree nodes that only affect the wildmen added by RimWU.
