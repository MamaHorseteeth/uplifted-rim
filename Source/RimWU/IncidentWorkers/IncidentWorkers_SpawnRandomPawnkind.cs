﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using Verse.Sound;

namespace RimWU
{
	//incidents ordered alphabetically

	public class IncidentWorker_FarmAnimalPeopleWanderIn : IncidentWorker
	{

		private const string FarmAnimalTradeTag = "AnimalFarm";

		//original value: .35f
		//this was untenable because the way i average wildness, only certain taurs would be eligible under max wildness constraints
		//new value is thus .5 + (.35/2) , to ensure that the same proportion of anthros specifically are eligible
		//necessarily this entails that no demis are eligible and an expanded range of taurs are eligible compared to their bases, but frankly, i'm fine with that
		private const float MaxWildness = 0.675f;

		private const float TotalBodySizeToSpawn = 2.5f;

		private static SimpleCurve BaseChanceFactorByAnimalBodySizePerCapitaCurve = new SimpleCurve
		{
			{
				new CurvePoint(0f, 1.5f),
				true
			},
			{
				new CurvePoint(4f, 1f),
				true
			}
		};

		private const float SelectionChanceFactorIfExistingMatingPair = 0.5f;
		public override float BaseChanceThisGame
		{
			get
			{
				if (!ModsConfig.IdeologyActive || !IdeoUtility.AnyColonistWithRanchingIssue())
				{
					return base.BaseChanceThisGame;
				}
				return base.BaseChanceThisGame * IncidentWorker_FarmAnimalPeopleWanderIn.BaseChanceFactorByAnimalBodySizePerCapitaCurve.Evaluate(PawnUtility.PlayerAnimalBodySizePerCapita());
			}
		}

		protected override bool CanFireNowSub(IncidentParms parms)
		{
			if (!base.CanFireNowSub(parms))
			{
				return false;
			}
			Map map = (Map)parms.target;
			IntVec3 intVec;
			PawnKindDef pawnKindDef;
			return RCellFinder.TryFindRandomPawnEntryCell(out intVec, map, CellFinder.EdgeRoadChance_Animal, false, null) && this.TryFindRandomPawnKind(map, out pawnKindDef);
		}

		protected override bool TryExecuteWorker(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			IntVec3 intVec;
			if (!RCellFinder.TryFindRandomPawnEntryCell(out intVec, map, CellFinder.EdgeRoadChance_Animal, false, null))
			{
				return false;
			}
			PawnKindDef pawnKindDef;
			if (!this.TryFindRandomPawnKind(map, out pawnKindDef))
			{
				return false;
			}
			int num = Mathf.Clamp(GenMath.RoundRandom(((parms.totalBodySize > 0f) ? parms.totalBodySize : TotalBodySizeToSpawn) / pawnKindDef.RaceProps.baseBodySize), 2, 10);
			if (num >= 2)
			{
				this.SpawnAnimal(intVec, map, pawnKindDef, new Gender?(Gender.Female));
				this.SpawnAnimal(intVec, map, pawnKindDef, new Gender?(Gender.Male));
				num -= 2;
			}
			for (int i = 0; i < num; i++)
			{
				this.SpawnAnimal(intVec, map, pawnKindDef, null);
			}
			base.SendStandardLetter(parms.customLetterLabel ?? "LetterLabelFarmAnimalsWanderIn".Translate(pawnKindDef.GetLabelPlural(-1)).CapitalizeFirst(), parms.customLetterText ?? "LetterFarmAnimalsWanderIn".Translate(pawnKindDef.GetLabelPlural(-1)), LetterDefOf.PositiveEvent, parms, new TargetInfo(intVec, map, false), Array.Empty<NamedArgument>());
			return true;
		}

		private void SpawnAnimal(IntVec3 location, Map map, PawnKindDef pawnKind, Gender? gender = null)
		{
			IntVec3 loc = CellFinder.RandomClosewalkCellNear(location, map, 12, null);
			Pawn pawn = PawnGenerator.GeneratePawn(new PawnGenerationRequest(pawnKind, null, PawnGenerationContext.NonPlayer, -1, false, false, false, false, true, false, 1f, false, true, true, true, false, false, false, false, 0f, 0f, null, 1f, null, null, null, null, null, null, null, gender, null, null, null, null, null, false, false, false));
			GenSpawn.Spawn(pawn, loc, map, Rot4.Random, WipeMode.Vanish, false);

			pawn.SetFaction(Faction.OfPlayer, null);
		}

		private bool TryFindRandomPawnKind(Map map, out PawnKindDef kind)
		{
			//replaces x.RaceProps.Animal to search instead for pawnkinddefs that either (have my tamed wildman flag, because they are stated to be familiar with human contact) or (are .Animal AND are NOT flagged as a removed animal) as a def mod extension
			return (from x in DefDatabase<PawnKindDef>.AllDefs
					where (x.HasModExtension<TamedWildManFlag>() || (x.RaceProps.Animal && !x.HasModExtension<RemovedAnimalFlag>())) && (x.RaceProps.wildness < MaxWildness) && map.mapTemperature.SeasonAndOutdoorTemperatureAcceptableFor(x.race) && x.race.tradeTags.Contains(FarmAnimalTradeTag) && !x.RaceProps.Dryad
					select x).TryRandomElementByWeight((PawnKindDef k) => this.SelectionChance(k), out kind);
		}

		private float SelectionChance(PawnKindDef pawnKind)
		{
			//original num float: 0.42000002f
			//added .5 , because the least wild anthro has a .5 wildness; this allows you to get the same number from an anthro cow as the regular incidentworker gets from a regular cow
			float num = 0.92000002f - pawnKind.RaceProps.wildness;
			if (PawnUtility.PlayerHasReproductivePair(pawnKind))
			{
				num *= 0.5f;
			}
			return num;
		}
	}


	public class IncidentWorker_HerdPeopleMigration : IncidentWorker
    {
		
		private static readonly IntRange AnimalsCount = new IntRange(3, 5);

		private const float MinTotalBodySize = 4f; //i'm not sure if this is per-animal or for the whole herd. the latter makes more sense, because the only animal i'm aware of being that big is the thrumbo

		protected override bool CanFireNowSub(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			PawnKindDef pawnKindDef;
			IntVec3 intVec;
			IntVec3 intVec2;
			return this.TryFindAnimalKind(map.Tile, out pawnKindDef) && this.TryFindStartAndEndCells(map, out intVec, out intVec2);
		}

		protected override bool TryExecuteWorker(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			PawnKindDef pawnKindDef;
			if (!this.TryFindAnimalKind(map.Tile, out pawnKindDef))
			{
				return false;
			}
			IntVec3 intVec;
			IntVec3 near;
			if (!this.TryFindStartAndEndCells(map, out intVec, out near))
			{
				return false;
			}
			Rot4 rot = Rot4.FromAngleFlat((map.Center - intVec).AngleFlat);
			List<Pawn> list = this.GenerateAnimals(pawnKindDef, map.Tile);
			for (int i = 0; i < list.Count; i++)
			{
				Thing newThing = list[i];
				IntVec3 loc = CellFinder.RandomClosewalkCellNear(intVec, map, 10, null);
				GenSpawn.Spawn(newThing, loc, map, rot, WipeMode.Vanish, false);
			}
			LordMaker.MakeNewLord(null, new LordJob_ExitMapNear(near, LocomotionUrgency.Walk, 12f, false, false), map, list);
			string str = string.Format(this.def.letterText, pawnKindDef.GetLabelPlural(-1)).CapitalizeFirst();
			string str2 = string.Format(this.def.letterLabel, pawnKindDef.GetLabelPlural(-1).CapitalizeFirst());
			base.SendStandardLetter(str2, str, this.def.letterDef, parms, list[0], Array.Empty<NamedArgument>());
			return true;
		}

		private bool TryFindAnimalKind(int tile, out PawnKindDef animalKind)
		{
			//changes k.RaceProps.CanDoHerdMigration (which requires .Animal to be true) to instead look for pawnkind defs with EITHER:
			//my specific wildman flag
			//AND k.RaceProps.herdMigrationAllowed (this ensures that only wildmen wander through and only those which are racially allowed to; no thrumbo-people, for instance)
			//OR
			//using vanilla CanDoHerdMigration (so it can grab animals from mods that i don't cover)
			//AND doesn't have my flag for animals i've replaced
			//we could probably figure out a way to allow it to do this with animals we haven't covered with the mod, but we can consider that later
			return (from k in DefDatabase<PawnKindDef>.AllDefs
					where ((k.HasModExtension<UntamedWildManFlag>() && k.RaceProps.herdMigrationAllowed) || (k.RaceProps.CanDoHerdMigration && !k.HasModExtension<RemovedAnimalFlag>()))
					&& Find.World.tileTemperatures.SeasonAndOutdoorTemperatureAcceptableFor(tile, k.race)
					select k).TryRandomElementByWeight((PawnKindDef x) => Mathf.Lerp(0.2f, 1f, x.RaceProps.wildness), out animalKind);
		}

		private bool TryFindStartAndEndCells(Map map, out IntVec3 start, out IntVec3 end)
		{
			if (!RCellFinder.TryFindRandomPawnEntryCell(out start, map, CellFinder.EdgeRoadChance_Animal, false, null))
			{
				end = IntVec3.Invalid;
				return false;
			}
			end = IntVec3.Invalid;
			for (int i = 0; i < 8; i++)
			{
				IntVec3 startLocal = start;
				IntVec3 intVec;
				if (!CellFinder.TryFindRandomEdgeCellWith((IntVec3 x) => map.reachability.CanReach(startLocal, x, PathEndMode.OnCell, TraverseParms.For(TraverseMode.NoPassClosedDoors, Danger.Deadly, false, false, false).WithFenceblocked(true)), map, CellFinder.EdgeRoadChance_Ignore, out intVec))
				{
					break;
				}
				if (!end.IsValid || intVec.DistanceToSquared(start) > end.DistanceToSquared(start))
				{
					end = intVec;
				}
			}
			return end.IsValid;
		}

		private List<Pawn> GenerateAnimals(PawnKindDef animalKind, int tile)
		{
			int num = IncidentWorker_HerdPeopleMigration.AnimalsCount.RandomInRange;

			num = Mathf.Max(num, Mathf.CeilToInt(MinTotalBodySize / animalKind.RaceProps.baseBodySize));
			List<Pawn> list = new List<Pawn>();
			for (int i = 0; i < num; i++)
			{
				Pawn item = PawnGenerator.GeneratePawn(new PawnGenerationRequest(animalKind, null, PawnGenerationContext.NonPlayer, tile, false, false, false, false, true, false, 1f, false, true, true, true, false, false, false, false, 0f, 0f, null, 1f, null, null, null, null, null, null, null, null, null, null, null, null, null, false, false, false));
				list.Add(item);
			}
			return list;
		}
		
	}

}