﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using Verse.Sound;

namespace RimWU
{
	public class IncidentWorker_WildManSelfTame : IncidentWorker
	{
		private IEnumerable<Pawn> Candidates(Map map)
		{
			return from x in map.mapPawns.AllPawnsSpawned
					   //using an or operator to allow modded animals on the map to be subject to random self-tamings
				   where (x.RaceProps.Animal || x.def.HasModExtension<RaceTaxonomy>()) && x.Faction == null && !x.Position.Fogged(x.Map) && !x.InMentalState && !x.Downed && x.RaceProps.wildness > 0f
				   select x;
		}

		protected override bool CanFireNowSub(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			return this.Candidates(map).Any<Pawn>();
		}

		protected override bool TryExecuteWorker(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			Pawn pawn = null;
			if (!this.Candidates(map).TryRandomElement(out pawn))
			{
				return false;
			}
			if (pawn.guest != null)
			{
				pawn.guest.SetGuestStatus(null, GuestStatus.Guest);
			}
			string value = pawn.LabelIndefinite();
			bool flag = pawn.Name != null;
			pawn.SetFaction(Faction.OfPlayer, null);
			string str;
			if (!flag && pawn.Name != null)
			{
				if (pawn.Name.Numerical)
				{
					str = "LetterAnimalSelfTameAndNameNumerical".Translate(value, pawn.Name.ToStringFull, pawn.Named("ANIMAL")).CapitalizeFirst();
				}
				else
				{
					str = "LetterAnimalSelfTameAndName".Translate(value, pawn.Name.ToStringFull, pawn.Named("ANIMAL")).CapitalizeFirst();
				}
			}
			else
			{
				str = "LetterAnimalSelfTame".Translate(pawn).CapitalizeFirst();
			}
			base.SendStandardLetter("LetterLabelAnimalSelfTame".Translate(pawn.KindLabel, pawn).CapitalizeFirst(), str, LetterDefOf.PositiveEvent, parms, pawn, Array.Empty<NamedArgument>());
			return true;
		}
	}

	public class IncidentWorker_WildManInsanityMass : IncidentWorker
	{
		public static bool AnimalUsable(Pawn p)
		{
			return p.Spawned && !p.Position.Fogged(p.Map) && (!p.InMentalState || !p.MentalStateDef.IsAggro) && !p.Downed && p.Faction == null;
		}
		public static void DriveInsane(Pawn p)
		{
			p.mindState.mentalStateHandler.TryStartMentalState(MentalStateDefOf.Manhunter, null, true, false, null, false, false, false);
		}
		protected override bool TryExecuteWorker(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			if (parms.points <= 0f)
			{
				Log.Error("AnimalInsanity running without points.");
				parms.points = (float)((int)(map.strengthWatcher.StrengthRating * 50f));
			}
			float adjustedPoints = parms.points;
			if (adjustedPoints > 250f)
			{
				adjustedPoints -= 250f;
				adjustedPoints *= 0.5f;
				adjustedPoints += 250f;
			}
			PawnKindDef animalDef;
			if (!(from def in DefDatabase<PawnKindDef>.AllDefs
					  //allows not only animals on the map but also wildmen both vanilla and rimwu
				  where (def.RaceProps.Animal || def.HasModExtension<UntamedWildManFlag>() || def.HasModExtension<VanillaWildManFlag>()) && def.combatPower <= adjustedPoints && (from p in map.mapPawns.AllPawnsSpawned
																																												  where p.kindDef == def && IncidentWorker_AnimalInsanityMass.AnimalUsable(p)
																																												  select p).Count<Pawn>() >= 3
				  select def).TryRandomElement(out animalDef))
			{
				return false;
			}
			List<Pawn> list = (from p in map.mapPawns.AllPawnsSpawned
							   where p.kindDef == animalDef && IncidentWorker_WildManInsanityMass.AnimalUsable(p)
							   select p).ToList<Pawn>();
			float combatPower = animalDef.combatPower;
			float num = 0f;
			int num2 = 0;
			Pawn pawn = null;
			list.Shuffle<Pawn>();
			foreach (Pawn pawn2 in list)
			{
				if (num + combatPower > adjustedPoints)
				{
					break;
				}
				IncidentWorker_WildManInsanityMass.DriveInsane(pawn2);
				num += combatPower;
				num2++;
				pawn = pawn2;
			}
			if (num == 0f)
			{
				return false;
			}
			string str;
			string str2;
			LetterDef baseLetterDef;
			if (num2 == 1)
			{
				str = "LetterLabelAnimalInsanitySingle".Translate(pawn.LabelShort, pawn.Named("ANIMAL")).CapitalizeFirst();
				str2 = "AnimalInsanitySingle".Translate(pawn.LabelShort, pawn.Named("ANIMAL")).CapitalizeFirst();
				baseLetterDef = LetterDefOf.ThreatSmall;
			}
			else
			{
				str = "LetterLabelAnimalInsanityMultiple".Translate(animalDef.GetLabelPlural(-1)).CapitalizeFirst();
				str2 = "AnimalInsanityMultiple".Translate(animalDef.GetLabelPlural(-1)).CapitalizeFirst();
				baseLetterDef = LetterDefOf.ThreatBig;
			}
			base.SendStandardLetter(str, str2, baseLetterDef, parms, pawn, Array.Empty<NamedArgument>());
			SoundDefOf.PsychicPulseGlobal.PlayOneShotOnCamera(map);
			if (map == Find.CurrentMap)
			{
				Find.CameraDriver.shaker.DoShake(1f);
			}
			return true;
		}
	}

	public class IncidentWorker_WildManInsanitySingle : IncidentWorker
	{
		private const int FixedPoints = 30;

		protected override bool CanFireNowSub(IncidentParms parms)
		{
			if (!base.CanFireNowSub(parms))
			{
				return false;
			}
			Map map = (Map)parms.target;
			Pawn pawn;
			return this.TryFindRandomAnimal(map, out pawn);
		}

		protected override bool TryExecuteWorker(IncidentParms parms)
		{
			Map map = (Map)parms.target;
			Pawn pawn;
			if (!this.TryFindRandomAnimal(map, out pawn))
			{
				return false;
			}
			IncidentWorker_AnimalInsanityMass.DriveInsane(pawn);
			string str = "AnimalInsanitySingle".Translate(pawn.Label, pawn.Named("ANIMAL")).CapitalizeFirst();
			base.SendStandardLetter("LetterLabelAnimalInsanitySingle".Translate(pawn.Label, pawn.Named("ANIMAL")).CapitalizeFirst(), str, LetterDefOf.ThreatSmall, parms, pawn, Array.Empty<NamedArgument>());
			return true;
		}

		private bool TryFindRandomAnimal(Map map, out Pawn animal)
		{
			int maxPoints = 150;
			if (GenDate.DaysPassedSinceSettle < 7)
			{
				maxPoints = 40;
			}
			return (from p in map.mapPawns.AllPawnsSpawned
						//allows it to target any animals on the map, vanilla wildmen, or my wildmen
					where (p.RaceProps.Animal || p.kindDef.HasModExtension<UntamedWildManFlag>() || p.kindDef.HasModExtension<VanillaWildManFlag>()) && p.kindDef.combatPower <= (float)maxPoints && IncidentWorker_WildManInsanityMass.AnimalUsable(p)
					select p).TryRandomElement(out animal);
		}
	}
}
