﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimWU
{
    //while shearable humanlikes  obviously need help to shear themselves in all the hard-to-reach places, my belief is that the typical milkable humanlike is both capable of and more comfortable with self-milking
	//as such, i am creating this comp so i don't have to use egglayer to simulate milkable humanlikes as i have been doing
	//one facet of the implementation of this is that i copy much from CompEggLayer instead of CompMilkable, because EggLayer extends directly from ThingComp while Milkable extends instead from CompHasGatherableBodyResource
	//minor modifications to copied parts (renaming things, targeting my own parallel CompProperties) are not noted in the below code
	internal class CompSelfMilkable : ThingComp
    {
		private float milkProgress;

		//modified from CompMilkable
		private bool Active
		{
			get
			{
				//CompMilkable first returns false if base.Active is false; i presume this is because we don't want to bog down the game with concerns about milking & shearing non-faction or suspended pawns, which nothing can be done about while they're not in the player faction
				//CompEggLayer doesn't have these because they need to lay eggs on their own, in order to, for instance, fill your map with wild turkeys, or ecosystem-destroying feralisks if you're playing alpha animals
				//my judgement is that it'd be annoying if wild boomalopefolk constantly left inedible, non-spoiling buckets of chemfuel all over the map, especially if they weren't autoforbidden
				//so the contents of base.Active have been transplanted here
				if (this.parent.Faction == null || this.parent.Suspended)
                {
					return false;
                }
				Pawn pawn = this.parent as Pawn;
				return (!this.Props.milkFemaleOnly || pawn == null || pawn.gender == Gender.Female) && (pawn == null || pawn.ageTracker.CurLifeStage.milkable);
			}
		}

		//copied from CompEggLayer
		public bool CanSelfMilkNow
		{
			get
			{
				return this.Active && this.milkProgress >= 1f;
			}
		}

		//copied from CompEggLayer
		public CompProperties_SelfMilkable Props
		{
			get
			{
				return (CompProperties_SelfMilkable)this.props;
			}
		}

		//copied from CompEggLayer
		public override void PostExposeData()
		{
			base.PostExposeData();
			Scribe_Values.Look<float>(ref this.milkProgress, "milkProgress", 0f, false);
		}

		//copied from CompEggLayer
		public override void CompTick()
		{
			if (this.Active)
			{
				float num = 1f / (this.Props.milkIntervalDays * 60000f);
				Pawn pawn = this.parent as Pawn;
				if (pawn != null)
				{
					num *= PawnUtility.BodyResourceGrowthSpeed(pawn);
				}
				this.milkProgress += num;
				if (this.milkProgress > 1f)
				{
					this.milkProgress = 1f;
				}
			}
		}

		//copied from CompEggLayer
		public ThingDef NextMilkType()
		{
			//check for fertilization count removed due to irrelevancy
			return this.Props.milkDef;
		}

		//copied from CompEggLayer
		public virtual Thing ProduceMilk()
		{
			if (!this.Active)
			{
				Log.Error("ProduceMilk while not Active: " + this.parent);
			}
			this.milkProgress = 0f;
			int randomInRange = this.Props.milkAmountRange.RandomInRange;
			if (randomInRange == 0)
			{
				return null;
			}
			Thing thing;
			//removed an if statement referring to fertilization and placed the else contents in the main code
			thing = ThingMaker.MakeThing(this.Props.milkDef, null);
			thing.stackCount = randomInRange;
			//removed code accessing CompHatcher. i mean, maybe i'll want to make some weird alien milk that "hatches" into a slime monster or something sometime down the line, but for now, it's just not necessary
			return thing;
		}

		//copied from CompEggLayer
		public override string CompInspectStringExtra()
		{
			if (!this.Active)
			{
				return null;
			}
			string text = "MilkFullness".Translate() + ": " + this.milkProgress.ToStringPercent();
			//removed irrelevant code for fertilization
			return text;
		}

	}

	public class CompProperties_SelfMilkable : CompProperties
    {
		//egglayer types it as a float, milkable types it as an int. i prefer the former for this
		public float milkIntervalDays = 1f;

		//vanilla milkables type milkAmount as an int
		//i just personally want to be able to set a range with a difference of one, since my animal people yield their milk in halves and quarters of the original amounts
		//this often means that the amount i need is a decimal value in a context where a fraction of a milk does not make sense
		public IntRange milkAmountRange = IntRange.one;

		public ThingDef milkDef;
		
		public bool milkFemaleOnly = true;

		public CompProperties_SelfMilkable()
        {
			this.compClass = typeof(CompSelfMilkable);
        }
	}

	//copied from JobGiver_LayEgg
	public class JobGiver_MilkSelf : ThinkNode_JobGiver
    {
		private const float SeekPrivacyRadius = 5f;

		private const float MaxSearchRadius = 30f;

		private const int MinSearchRegions = 10;

		protected override Job TryGiveJob(Pawn pawn)
		{
			CompSelfMilkable compSelfMilkable = pawn.TryGetComp<CompSelfMilkable>();
			if (compSelfMilkable == null || !compSelfMilkable.CanSelfMilkNow)
			{
				return null;
			}
			ThingDef singleDef = compSelfMilkable.NextMilkType();
			PathEndMode peMode = PathEndMode.OnCell;
			TraverseParms traverseParms = TraverseParms.For(pawn, Danger.Some, TraverseMode.ByPawn, false, false, false);
			//near as i can tell, in its original context, this code is the difference between a player chicken finding an egg box to lay in if possible and a wild chicken disregarding that entirely and just laying wherever
			//so i'm gonna just comment it out assuming i'm right, as i don't want cowpeople going to egg boxes to milk themselves
			/*if (pawn.Faction == Faction.OfPlayer)
			{
				Thing bestEggBox = this.GetBestEggBox(pawn, peMode, traverseParms);
				if (bestEggBox != null)
				{
					return JobMaker.MakeJob(JobDefOf.LayEgg, bestEggBox);
				}
			}*/

			Thing thing = GenClosest.ClosestThingReachable(pawn.Position, pawn.Map, ThingRequest.ForDef(singleDef), peMode, traverseParms, MaxSearchRadius, (Thing x) => pawn.GetRoom(RegionType.Set_All) == null || x.GetRoom(RegionType.Set_All) == pawn.GetRoom(RegionType.Set_All), null, 0, -1, false, RegionType.Set_Passable, false);
			return JobMaker.MakeJob(Common.MilkSelf, (thing != null) ? thing.Position : RCellFinder.RandomWanderDestFor(pawn, pawn.Position, SeekPrivacyRadius, null, Danger.Some));
		}

		//did this decompile wrong or something? i'll have to ask my mentor
		//anyways, since it's just to do with finding an egg box, i'm comfortable just commenting it out for now
		/*private Thing GetBestEggBox(Pawn pawn, PathEndMode peMode, TraverseParms tp)
		{
			JobGiver_LayEgg.<> c__DisplayClass4_0 CS$<> 8__locals1 = new JobGiver_LayEgg.<> c__DisplayClass4_0();
			CS$<> 8__locals1.pawn = pawn;
			CS$<> 8__locals1.eggDef = CS$<> 8__locals1.pawn.TryGetComp<CompEggLayer>().NextEggType();
			return GenClosest.ClosestThing_Regionwise_ReachablePrioritized(CS$<> 8__locals1.pawn.Position, CS$<> 8__locals1.pawn.Map, ThingRequest.ForDef(ThingDefOf.EggBox), peMode, tp, 30f, new Predicate<Thing>(CS$<> 8__locals1.< GetBestEggBox > g__IsUsableBox | 0), new Func<Thing, float>(JobGiver_LayEgg.<> c.<> 9.< GetBestEggBox > g__GetScore | 4_1), 10, 30);
		}*/

		
	}

	//copied from JobDriver_LayEgg
	public class JobDriver_MilkSelf : JobDriver
    {
		private const int MilkSelf = 500;

		private const TargetIndex LaySpotOrEggBoxInd = TargetIndex.A;

		/*public CompEggContainer EggBoxComp
		{
			get
			{
				return this.job.GetTarget(TargetIndex.A).Thing.TryGetComp<CompEggContainer>();
			}
		}*/

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.OnCell);
			yield return Toils_General.Wait(MilkSelf, TargetIndex.None);
			yield return Toils_General.Do(delegate
			{
				Thing thing = this.pawn.GetComp<CompSelfMilkable>().ProduceMilk();
				/*if (this.job.GetTarget(TargetIndex.A).HasThing && this.EggBoxComp.Accepts(thing.def))
				{
					this.EggBoxComp.innerContainer.TryAdd(thing, true);
					return;
				}*/
				Thing t = GenSpawn.Spawn(thing, this.pawn.Position, base.Map, WipeMode.Vanish);
				if (this.pawn.Faction != Faction.OfPlayer)
				{
					t.SetForbidden(true, true);
				}
			});
			yield break;
		}

		
	}

}
