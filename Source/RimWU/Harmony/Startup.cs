﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Verse;
using HarmonyLib;

namespace RimWU
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony.DEBUG = false;
            Harmony harmony = new Harmony("RimWU");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}
