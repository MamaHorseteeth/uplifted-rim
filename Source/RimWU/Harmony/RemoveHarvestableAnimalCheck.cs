﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using HarmonyLib;
using Verse;

namespace RimWU
{
    [HarmonyPatch(typeof(WorkGiver_GatherAnimalBodyResources), "ShouldSkip")]
    internal class WorkGiver_GatherAnimalBodyResources_ShouldNotSkipUpliftedWildMen
    {
        /// <summary>
        /// Extends the if(animal) condition with a check for uplifted animals
        /// </summary>
        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> AddUpliftedAnimalCheck(IEnumerable<CodeInstruction> instructions)
        {
            CodeInstruction insertionInstruction = instructions
                .First(c => c.Calls(AccessTools.Method(typeof(RaceProperties), "get_Animal")));    // find the access to field Animal

            foreach (CodeInstruction instruction in instructions)
            {
                if (instruction == insertionInstruction)
                {
                    yield return instruction;

                    yield return new CodeInstruction(OpCodes.Ldloc_0);  // load local variable list
                    yield return new CodeInstruction(OpCodes.Ldloc_1);  // load local variable i
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(List<Pawn>), "get_Item")); // list[i]
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(WorkGiver_GatherAnimalBodyResources_ShouldNotSkipUpliftedWildMen), "IsUpliftedAnimal"));  // call static method
                    yield return new CodeInstruction(OpCodes.Or);   // compare to base game result: if .Animal or .IsUpliftedAnimal are true
                }
                else
                {
                    yield return instruction;
                }
            }
        }

        public static bool IsUpliftedAnimal(Pawn pawn)
        {
            return pawn.def.HasModExtension<BodyResourcesFlag>();
        }
    }

    [HarmonyPatch(typeof(WorkGiver_GatherAnimalBodyResources), "HasJobOnThing")]
    internal class WorkGiver_GatherAnimalBodyResources_HasJobOnThing_ShouldAllowWildMen
    {
        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> AddUpliftedAnimalCheck(IEnumerable<CodeInstruction> instructions)
        {
            CodeInstruction insertionInstruction = instructions
                .First(c => c.Calls(AccessTools.Method(typeof(RaceProperties), "get_Animal")));

            foreach (CodeInstruction instruction in instructions)
            {
                if (instruction == insertionInstruction)
                {
                    yield return instruction;

                    yield return new CodeInstruction(OpCodes.Ldloc_0);  // load local variable pawn2
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(WorkGiver_GatherAnimalBodyResources_HasJobOnThing_ShouldAllowWildMen), "IsUpliftedAnimal"));  // call static method
                    yield return new CodeInstruction(OpCodes.Or);   // compare to base game result: if .Animal or .IsUpliftedAnimal are true

                }
                else
                {
                    yield return instruction;
                }
            }
        }

        public static bool IsUpliftedAnimal(Pawn pawn)
        {
            return pawn.def.HasModExtension<BodyResourcesFlag>();
        }
    }
}
