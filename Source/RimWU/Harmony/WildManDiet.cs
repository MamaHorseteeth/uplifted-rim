﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HarmonyLib;
using RimWorld;
using Verse;

namespace RimWU
{
    [HarmonyPatch(typeof (RaceProperties), "CanEverEat", new Type[] {typeof(ThingDef)} )]
    internal class WildManDiet
    {
        [HarmonyPrefix]
        private static bool WillEatWhileWild(ref RaceProperties __instance, ref bool __result, ref ThingDef t)
        {
            bool backupResult = __result;
            try
            {
                
                //if the instanced race properties' willNeverEat list is null (as it is by default), or it exists but does not contain t, such as if it is empty:
                //we don't need to touch anything and the base code can execute
                if (__instance.willNeverEat == null || !__instance.willNeverEat.Contains(t))
                {
                    return true;
                }

                //for however many pawnkinds have the defmodextensions DietWhenWildMan and UntamedWildManFlag
                //we can't use cachedAnyPawnKind here because that only gets the first pawnkind of the race, when we need to specifically get a pawnkind that has the modextensions
                for (int i = 0; i < DietUtility.GetCachedWildManDietPawnKind().Count; i++)
                {
                    //if the current pawnkind def's race properties correspond to this instance
                    //(i think that means that if we had two races that had the exact same race properties, you might run into trouble?
                    //but that would more or less require either oblivious def duplication, in which case this is the least of one's worries, or species to be intentionally so similar it wouldn't matter)
                    if (DietUtility.GetCachedWildManDietPawnKind()[i].race.race == __instance)
                    {
                        PawnKindDef pawnKind = DietUtility.GetCachedWildManDietPawnKind()[i];

                        //if willAlsoEat contains t
                        if ( pawnKind.GetModExtension<DietWhenWildMan>().willAlsoEat.Contains(t))
                        {
                            //we use a modified version of the base code that does not include the check that willNeverEat does not contain t /is null to determine the bool value of the original code
                            //because if it must be null, then this would never work, as this code is designed explicitly to allow certain pawnkinds to eat things that willNeverEat must exist in order to forbid
                            __result = __instance.EatsFood && t.ingestible != null && t.ingestible.preferability != FoodPreferability.Undefined && (__instance.willNeverEat == null) && __instance.Eats(t.ingestible.foodType);
                            //and we return false so that the original code doesn't execute
                            return false;
                        }

                        //else if willAlsoEat does not contain t, then this code is irrelevant and the base code should run as normal
                        return true;
                    }
                }
                //if the diet utility list is empty or does not contain any pawnkinds of the same race which have the requisite defmodextensions, then we don't need to worry about this and can allow the base code to execute
                return true;
            }
            catch (Exception e)
            {
                __result = backupResult;
                Log.Warning("RimWU: Something went wrong allowing a wildman pawnkind to eat things its race is otherwise not allowed to" + e);
                return true;
            }
        }
    }
}
