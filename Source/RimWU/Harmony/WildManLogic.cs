﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Reflection.Emit;

using RimWorld;
using Verse;
using UnityEngine;
using HarmonyLib;
using AlienRace;

namespace RimWU
{
    //this prefix targets the HAR prefix that prevents pawnkinds from changing on faction change unless the original or new pawnkind is vanilla WildMan. we need to do this so that our custom wildman pawnkinds can also be changed to and from
    [HarmonyPatch(typeof(HarmonyPatches), "ChangeKindPrefix")]
    static class HARChangeKindInterceptor
    {
        [HarmonyPostfix]
        private static void HARPawnKindPrefix_CustomWildManPrefix(Pawn __0, ref bool __result, ref PawnKindDef newKindDef)
        {
            bool backupResult = __result;
            try
            {
                //this is how you reference the __instance of a patch you're patching
                Pawn instance = __0;

                //we only care about our custom wildmen here, so skip my patch if it ain't one of ours, and let HAR do its thing
                if (instance.kindDef.HasModExtension<UntamedWildManFlag>())
                {
                    __result = true;
                }

                //else, we defer to whatever HAR wants to do
            }
            catch (Exception e)
            {
                __result = backupResult;
                Log.Warning("RimWU: broke something, somehow with HARPawnKindPrefix_CustomWildManPrefix:" + e);
            }
        }
    }

    //this prefix directly targets the changekind logic now that HAR has been told to stand aside, in order to tell rimworld what specific racial pawnkind we need
    [HarmonyPatch(typeof(Pawn), "ChangeKind")]
    static class WildManChangeKindLogic
    {
        [HarmonyPrefix]
        static void TameUpliftedWildMan(Pawn __instance, ref PawnKindDef newKindDef)
        {
            PawnKindDef upliftedKindDef = null;
            //this check comes into play if a vanilla human wildman is recruited, or some other mod also pre-empts the HAR prefix in the same way
            if (__instance.kindDef.HasModExtension<UntamedWildManFlag>())
            {
                //gets a custom "tamed" wildman pawnkind matching the race of the pawn in question
                upliftedKindDef = TamedPawnKindUtility.GetTamedPawnKind(__instance.def);
                if (upliftedKindDef != null)
                {
                    newKindDef = upliftedKindDef;
                }
            }

            //meanwhile, if the new pawnkind would normally be a wildman and __instance has the RaceTaxonomy mod extension (as all of our races do), we want the animal person to become the right pawnkind of wildman, not the human wildman
            //by specifying RaceTaxonomy, we avoid grabbing other race mods' races
            if (newKindDef == PawnKindDefOf.WildMan && __instance.def.HasModExtension<RaceTaxonomy>())
            {
                //gets the untamed wildman pawnkind matching the race of the pawn in question
                upliftedKindDef = TamedPawnKindUtility.GetUntamedPawnKind(__instance.def);
                if (upliftedKindDef != null)
                {
                    newKindDef = upliftedKindDef;
                }
            }
        }
    }

    //this postfix makes it so that my custom wildmen are acknowledged as wildmen for the purposes of, for example, taming and hunting
    [HarmonyPatch(typeof(WildManUtility), "IsWildMan")]
    static class IsWildManIfFlagged
    {
        [HarmonyPostfix]
        private static void AddUpliftedAnimalPawnKinds(ref bool __result, Pawn p)
        {
            bool backupResult = __result;
            try
            {
                //saves a bit of performance by ending the patch if the game has already determined the pawnkind is a vanilla wildman
                if(__result == true)
                {
                    return;
                }

                //gets a yes or no about whether the pawnkind has this flag
                bool isUpliftedWildMan = p.kindDef.HasModExtension<UntamedWildManFlag>();
                //sets the IsWildMan bool to true
                __result = isUpliftedWildMan;
            }
            catch (Exception e)
            {
                __result = backupResult;
                Log.Warning("RimWU: something went wrong adding RimWU wildman pawnkind def: " + p.kindDef + "to isWildman. Error: " + e);
            }
        }
    }

    //this postfix allows not only my custom wildmen, but vanilla human wildmen too if enabled, to show up as manhunters
    [HarmonyPatch(typeof(ManhunterPackIncidentUtility), "CanArriveManhunter")]
    static class WildManEligibleForManhunter
    {
        [HarmonyPostfix]
        private static void AddWildManToManhunter(ref bool __result, ref PawnKindDef kind)
        {
            bool backupResult = __result;
            try
            {
                //if the game has already determined a pawnkind to be eligible, then permit it to continue being a manhunter, in order to defer to mods that add animals we don't cover
                //to disqualify base animals covered by the mod, we use xml to patch canArriveManhunter to false instead
                if (__result == true)
                {
                    return;
                }

                //if it has either the mod extension that marks one of my wildmen OR BOTH the mod extension that i add to vanilla wildmen and the extend-to-vanilla setting enabled AND
                //if it can arrive manhunter AND
                //if it can pass fences
                bool canArriveManhunter = (kind.HasModExtension<UntamedWildManFlag>()
                    || (kind.HasModExtension<VanillaWildManFlag>() && RimWildlifeUplifted.settings.extendRimWUBehaviorToVanillaWildmen == true))
                    && kind.canArriveManhunter && kind.RaceProps.CanPassFences;
                __result = canArriveManhunter;
            }
            catch (Exception e)
            {
                __result = backupResult;
                Log.Warning("RimWU: Something went wrong adding wildmen to manhunter selection" + e);
            }
        }
    }

    //this transpiler gets the forced .75f wildness out of the way for all of my wildmen and, if enabled, vanilla wildmen too
    //thanks to CnArmor for providing code to study
    [HarmonyPatch(typeof(InteractionWorker_RecruitAttempt), "Interacted")]
    internal class InteractionWorker_RecruitAttempt_IgnoreFixedWildManWildnessIfUpliftedAnimal
    {
        [HarmonyTranspiler]
        private static IEnumerable<CodeInstruction> IgnoreFixedWildnessIfUpliftedAnimal(IEnumerable<CodeInstruction> instructions)
        {
            MethodInfo isWildMan = AccessTools.Method(typeof(WildManUtility), "IsWildMan"); //assigns the target call for IsWildMan to a methodinfo variable
            MethodInfo isWildManButNotUpliftedAnimal = AccessTools.Method(typeof(InteractionWorker_RecruitAttempt_IgnoreFixedWildManWildnessIfUpliftedAnimal), "IsWildManButNotUpliftedAnimal"); //assigns the replacement call as well, defined below
            List<CodeInstruction> codes = instructions.ToList(); //gets the list of instructions
            foreach (CodeInstruction code in codes)
            {
                if (code.Calls(isWildMan))
                {
                    //replaces the check to exclude all pawnkinds specifically flagged as my untamed wildmen
                    code.operand = isWildManButNotUpliftedAnimal;
                }
                yield return code;
            }
        }

        public static bool IsWildManButNotUpliftedAnimal(Pawn pawn)
        {
            //(if the pawn is a wildman AND does not have the mod extension) AND the rimwu wildman behaviors are not extended to vanilla wildmen
            return (pawn.IsWildMan() && !pawn.kindDef.HasModExtension<UntamedWildManFlag>()) && RimWildlifeUplifted.settings.extendRimWUBehaviorToVanillaWildmen == false;
        }
    }
}
