﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimWU
{

    // ANIMALS

    //this postfix sets the ideology of a spawned pawn if that spawned pawn has a mod extension marking which faction it should take its ideology from
    //it could have been a transpiler, but i frankly don't know enough about transpilers to know how to have made it that way, with such a complicated method as TryGenerateNewPawnInternal
    [HarmonyPatch(typeof(PawnGenerator), "TryGenerateNewPawnInternal")]
    internal class PawnGenerator_SetIdeoIfUpliftedWildMan
    {
        [HarmonyPostfix]
        public static void SetIdeoligeonToIdeoligionFromFaction(ref Pawn __result)
        {
            if (__result == null)
            {
                //if the pawn wasn't generated, then we don't need to do anything. exit immediately
                return;
            }

            Pawn backupResult = __result;
            try
            {
                ////if the pawnkind is marked to get an ideoligeon from a faction and it hasn't been set yet
                //if (__result.kindDef.HasModExtension<HasFactionIdeoligion>() == true)
                //{
                    
                //    //allows easy access to the mod extension's props
                //    HasFactionIdeoligion IdeoFactionProps = __result.kindDef.GetModExtension<HasFactionIdeoligion>();

                //    //get a random faction of those instanced from the def pointed to in the mod extension. unless i change my mind about how to handle diversity of thought among wild animals, there should only be one faction to pick from
                //    Faction faction = FactionUtility.DefaultFactionFrom(IdeoFactionProps.ideologyFromFaction);
                //    //if that faction does not have an ideology somehow. idk how this might happen
                //    if (faction.ideos == null)
                //    {
                //        Log.Error("RimWU: FactionDef " + faction.def + "has no ideology! Possible solutions: add <classicIdeo> entry to FactionDef");
                //        //there's nothing more we can do here, so return
                //        return;
                //    }
                //    //if that faction has any ideologies (it shoud, if the defs are constructed right)
                //    if (faction.ideos != null)
                //    {
                //        //then set the pawn to have a random ideo from that faction (having only the one ideology)
                //        __result.ideo.SetIdeo(faction.ideos.GetRandomIdeoForNewPawn());
                //    }
                //}
            }
            catch (Exception e)
            {
                __result = backupResult;
                Log.Warning("RimWU: Something went wrong adding an ideology to a freshly-generated wildman pawn. Pawn name: " + __result.Name + " and exception: " + e);
                return;
            }
        }
    }

    


    // INSECTOIDS & MECHANOIDS

    //this postfix targets the faction manager to change the factions defs RimWorld's code considers to be the Insects faction and Mechanoids faction
    //it technically could be a transpiler instead but i don't think it makes much of a performance difference
    [HarmonyPatch(typeof(FactionManager), "RecacheFactions")]
    static class FactionManager_RecacheFactions
    {
        [HarmonyPostfix]
        private static void SetFactionsToHivePeopleAndMechPeople(ref FactionManager __instance, ref Faction ___ofInsects, ref Faction ___ofMechanoids)
        {
            try
            {
                ___ofInsects = __instance.FirstFactionOfDef(Common.InsectPeopleHiveFaction);
                ___ofMechanoids = __instance.FirstFactionOfDef(Common.MechaPeopleNetworkFaction);
            }
            catch (Exception e)
            {
                Log.Warning("RimWU: Something went wrong changing default insectoid and mechanoid factions" + e);
                return;
            }
        }
    }

    //this prefix prevents code from running which resets what hives are allowed to spawn to the vanilla pawnkinds
    [HarmonyPatch(typeof(Hive), "ResetStaticData")]
    static class SpawnerLogic_HiveResetStaticData
    {
        [HarmonyPrefix]
        private static bool SpawnerLogic_DoNotResetHiveStaticData()
        {
            //we just don't want this code to run at all
            return false;
        }
    }

    //this postfix changes the suitable pawnkinds for a cluster raid to consider if their default faction is our mechanoid faction, while still also making sure they're not good breachers
    [HarmonyPatch(typeof(MechClusterGenerator), "MechKindSuitableForCluster")]
    static class MechClusterGenerator_MechKindSuitableForCluster
    {
        [HarmonyPostfix]
        private static void ClusterGenerator_CheckForMechFactionInsteadOfFleshType(ref bool __result, ref PawnKindDef def)
        {
            {
                bool backupResult = __result;
                try
                {
                    bool isMechanoidNetworkDrone = (def.defaultFactionType == Common.MechaPeopleNetworkFaction && !def.isGoodBreacher);
                    __result = isMechanoidNetworkDrone;
                }
                catch (Exception e)
                {
                    __result = backupResult;
                    Log.Warning("RimWU: Something went wrong changing mechanoid cluster generation to check pawnkind faction " + e);
                    return;
                }
            }
        }
    }

}
