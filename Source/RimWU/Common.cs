﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;

namespace RimWU
{

    //Flags

    public class WildAnimalFactionFlag : DefModExtension { } //FactionDef . used to mark factions that count as normal (non-insectoid, non-mechanoid) factions

    public class RemovedAnimalFlag : DefModExtension { } //PawnKindDef . used to mark pawnkinds that are supposed to be patched out of the game

    public class TamedWildManFlag : DefModExtension { } //PawnKindDef . used to mark that a pawnkind is the "tamed" version a wildman becomes
    public class UntamedInsectoidHiveFlag : DefModExtension { } //PawnKindDef . used to mark that a pawnkind is spawned by insectoid events; mostly used to fall back on wildman behavior if downed
    public class UntamedMechanoidNetworkFlag : DefModExtension { } //PawnKindDef . used to mark that a pawnkind is spawned by mechanoid events; mostly used to fall back on wildman behavior if downed
    public class UntamedWildManFlag : DefModExtension { } //PawnKindDef . used to mark that a pawnkind obeys wildman logic
    public class VanillaWildManFlag : DefModExtension { } //PawnKindDef . patched into the vanilla wildman to allow code to manipulate them too

    public class HasFactionIdeoligion : DefModExtension //PawnKindDef
    {
        //the faction def whose ideology needs to be applied to the pawn
        public FactionDef ideologyFromFaction = null;
    }

    public class AnimalResourceCruel : DefModExtension { } //ThingDef_Item . used to mark that an item can only be acquired through cruelty

    public class BodyResourcesFlag : DefModExtension { } //ThingDef_Race . used to mark that a race has a harvestable resource

    //Extensions
    public class RaceTaxonomy : DefModExtension //ThingDef_Race
    {
        //the base animal that all members of the taxon share; i.e., demi-cobras, anthro-cobras, and cobra-lamias all form the RimWU_Cobra taxon. this is used for translations (so RimWU_Cobra => "cobra-people")
        public string raceTaxon;
        //the specific subgenre of furry a race is
        // Demi: has the ears, tail, horns, and antennae but otherwise looks human. most stats are an average between 3 human bases and the animal base
        // Anthro: has all the qualities of the animal, but transplanted onto a bipedal humanoid skeleton. most stats are an average between the human and animal bases
        // Demitaur: has the lower body of the animal but the upper body of a demi. most stats are an average between the human and animal bases
        // Taur: has the lower body of the animal but the upper body of an anthro. most stats are an average between the human base and 3 animal bases
        //i tried to make it an enum but that didn't seem to work so whatever
        public string raceSubTaxon;
        //public string raceTaxonTip;
        public bool defaultEnabled = true; //whether the species is enabled or not by default. i don't really plan on changing this in the xml, but the option is here

        public bool isMechanoid = false; //whether the species counts as mechanoid for the purposes of incorporating mechanoid mechanics
    }

    public class DietWhenWildMan : DefModExtension //PawnKind
    {
        public List<ThingDef> willAlsoEat = new List<ThingDef> { }; //might need to declare a new list here, idk
    }


    public static class Common
    {
        //RimWU versions of factions
        public static FactionDef InsectPeopleHiveFaction = DefDatabase<FactionDef>.GetNamed("RimWU_InsectoidHivePeople");
        public static FactionDef MechaPeopleNetworkFaction = DefDatabase<FactionDef>.GetNamed("RimWU_MechanoidNetworkPeople");

        //basically just gets a list of all races associated with the Rim_WU project
        public static List<AlienRace.ThingDef_AlienRace> raceTaxa = DefDatabase<AlienRace.ThingDef_AlienRace>.AllDefsListForReading
            .Where(raceDef => raceDef.HasModExtension<RaceTaxonomy>()).ToList();
        

        //incident worker patching for thrumbos wander in event
        public static AlienRace.ThingDef_AlienRace ThrumboPersonRace = DefDatabase<AlienRace.ThingDef_AlienRace>.GetNamed("RimWU_AnimalPerson_Thrumbo_Anthro");
        public static PawnKindDef ThrumboWildMan = DefDatabase<PawnKindDef>.GetNamed("RimWU_WildMan_AnimalPerson_Thrumbo_Anthro");

        //incident worker patching for alphabeavers event
        public static AlienRace.ThingDef_AlienRace AlphabeaverPersonRace = DefDatabase<AlienRace.ThingDef_AlienRace>.GetNamed("RimWU_AnimalPerson_Alphabeaver_Anthro");
        public static PawnKindDef AlphabeaverWildMan = DefDatabase<PawnKindDef>.GetNamed("RimWU_WildMan_AnimalPerson_Alphabeaver_Anthro");

        //needed for job giver for self-milking
        public static JobDef MilkSelf = DefDatabase<JobDef>.GetNamed("RimWU_MilkSelf");

        //list of all "cruel" leathers
        public static List<ThingDef> CruelAnimalResourcesList = DefDatabase<ThingDef>.AllDefsListForReading
            .Where(resource => resource.HasModExtension<AnimalResourceCruel>()).ToList();
    }

    
}
