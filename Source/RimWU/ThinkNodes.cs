﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;
using Verse.AI;

namespace RimWU
{
    public class ThinkNode_ConditionalHasWildManModExtension : ThinkNode_Conditional
    {
        protected override bool Satisfied(Pawn pawn)
        {
            //we also allow insectoid hive and mechanoid network pawns to engage in this behavior because it only fires for them after their lordjob from spawning goes away
            //that is to say, if they get downed but heal enough on their own to get back up
            //if we don't, then they walk off the map at their nearest convenience, which is:
            //avanillate for insectoids (normally wander around like regular animals after recovery)
            //and unhandled territory for mechanoids (normally die on downed)

            return pawn.kindDef.HasModExtension<UntamedWildManFlag>()
                || pawn.kindDef.HasModExtension<UntamedInsectoidHiveFlag>()
                || pawn.kindDef.HasModExtension<UntamedMechanoidNetworkFlag>()
                || (pawn.kindDef.HasModExtension<VanillaWildManFlag>() && RimWildlifeUplifted.settings.extendRimWUBehaviorToVanillaWildmen == true)
                ;
        }
    }

    public class ThinkNode_ConditionalWildManWrongSeason : ThinkNode_Conditional
    {
        protected override bool Satisfied(Pawn pawn)
        {
            return pawn.IsWildMan() && !pawn.Map.mapTemperature.SeasonAcceptableFor(pawn.def);
        }
    }
}
