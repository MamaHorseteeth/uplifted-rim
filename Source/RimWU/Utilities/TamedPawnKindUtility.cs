﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;

namespace RimWU
{
    internal class TamedPawnKindUtility
    {
        private static Dictionary<ThingDef, PawnKindDef> cachedTamedPawnKinds = new Dictionary<ThingDef, PawnKindDef>();
        public static PawnKindDef GetTamedPawnKind(ThingDef race)
        {
            if (!cachedTamedPawnKinds.ContainsKey(race))
            {
                IEnumerable<PawnKindDef> tamedPawnKinds = DefDatabase<PawnKindDef>.AllDefsListForReading
                .Where(pawnKindDef => pawnKindDef.HasModExtension<TamedWildManFlag>() && pawnKindDef.race == race);
                // checks to see if we got exactly one
                // in the future we might want to tailor some custom pawnkinds to the vanilla starting factions, but that's a lot of code for very little impact
                if (tamedPawnKinds.Count() < 1)
                {
                    Log.Warning("RimWU: Found no pawnkinds with Tamed flag for race: " + race + " . Retaining current PawnKind.");
                    return null;
                }
                if (tamedPawnKinds.Count() > 1)
                {
                    Log.Warning("RimWU: More than one Tamed pawnkind def found for race: " + race + " . Using first in list.");
                }
                //adds the race-pawnkind combination to the cache
                PawnKindDef value = tamedPawnKinds.First();
                cachedTamedPawnKinds.Add(race, value);
            }
            return cachedTamedPawnKinds[race];
        }

        private static Dictionary<ThingDef, PawnKindDef> cachedUntamedPawnKinds = new Dictionary<ThingDef, PawnKindDef>();
        public static PawnKindDef GetUntamedPawnKind(ThingDef race)
        {
            if (!cachedTamedPawnKinds.ContainsKey(race))
            {
                IEnumerable<PawnKindDef> untamedPawnKinds = DefDatabase<PawnKindDef>.AllDefsListForReading
                .Where(pawnKindDef => pawnKindDef.HasModExtension<UntamedWildManFlag>() && pawnKindDef.race == race);
                // checks to see if we got exactly one
                if (untamedPawnKinds.Count() < 1)
                {
                    Log.Warning("RimWU: Found no pawnkinds with Untamed flag for race: " + race + " . Retaining current PawnKind.");
                    return null;
                }
                if (untamedPawnKinds.Count() > 1)
                {
                    Log.Warning("RimWU: More than one Untamed pawnkind def found for race: " + race + " . Using first in list.");
                }
                //adds the race-pawnkind combination to the cache
                PawnKindDef value = untamedPawnKinds.First();
                cachedUntamedPawnKinds.Add(race, value);
            }
            return cachedUntamedPawnKinds[race];
        }
    }
}
