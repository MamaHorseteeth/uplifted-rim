﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HarmonyLib;
using RimWorld;
using Verse;

namespace RimWU
{
    internal class DietUtility
    {
        private static List<PawnKindDef> cachedWildManDietPawnKinds = new List<PawnKindDef>();
        public static List<PawnKindDef> GetCachedWildManDietPawnKind()
        {
            //if we haven't cached any pawnkind defs
            if (cachedWildManDietPawnKinds.Count == 0)
            {
                //gets all pawnkind defs that have the relevant mod extensions: one that marks it as a wildman and one that informs that it has special dietary considerations
                IEnumerable<PawnKindDef> wildManDietPawnKinds = DefDatabase<PawnKindDef>.AllDefsListForReading
                    .Where(pawnKindDef => pawnKindDef.HasModExtension<DietWhenWildMan>() && pawnKindDef.HasModExtension<UntamedWildManFlag>());
                // if there are no pawnkinds with the flags, then we don't need to populate the cache and can just return the list, which should be empty
                if (wildManDietPawnKinds.Count() <= 0)
                {
                    return cachedWildManDietPawnKinds;
                }
                //adds any new pawnkinds to the cache
                cachedWildManDietPawnKinds.Concat(wildManDietPawnKinds);
            }
            //returns the list, either because the cache was already populated this session or because we're done populating it
            return cachedWildManDietPawnKinds;
        }
    }
}
