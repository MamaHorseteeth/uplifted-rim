﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;
using HarmonyLib;

namespace RimWU
{
    
    
    //copied largely from Hediff_MissingPart or Hediff_Implant
    public class Hediff_Dimorphism : HediffWithComps
    {
        //not copying the .LabelBase or .LabelInBrackets from _MissingPart; using the virtual set forth in Hediff, like most hediffs
        //not copying the .BleedRate or .PainOffset from _MissingPart; these shouldn't do those under any circumstances, like most hediffs

        
        
        //copied directly from Hediff. kind of a useless override, but the idea is that dimorphic parts shouldn't impact health. if we change our minds, though, it'll be easy to edit/copy into here
        //public override float SummaryHealthPercentImpact
        //{
        //    get
        //    {
        //        return 0f;
        //    }
        //}

        //copied from _MissingPart in entirety. pretty sure this is used by healthticks to determine if the hediff should ever be removed over time. obviously, it's supposed to be permanent
        public override bool ShouldRemove
        {
            get
            {
                return false;
            }
        }

        //copied from _Implant
        public override void PostAdd(DamageInfo? dinfo)
        {
            base.PostAdd(dinfo);
            if (base.Part == null)
            {
                Log.Error(this.def.defName + " has null Part. It should be set before PostAdd.");
                return;
            }
        }

        //copied from _Implant in entirety
        public override void ExposeData()
        {
            base.ExposeData();
            if (Scribe.mode == LoadSaveMode.PostLoadInit && base.Part == null)
            {
                Log.Error(base.GetType().Name + " has null part after loading.");
                this.pawn.health.hediffSet.hediffs.Remove(this);
                return;
            }
        }
    }
    
    //thanks to nabber for massive assistance with this part
    public class HediffGiver_StartWithHediff : HediffGiver
    {
        //gendered chances are multiplicative against the base chance
        //base chance of .5 and male chance of .5 will result in an effective .25 chance of a hediff appearing at start on a male
        public float chance = 1f;
        public float maleCommonality = 1f;
        public float femaleCommonality = 1f;
        
        public bool affectsEveryPart = true;
        public float individualPartChance = 1f;

        public void GiveHediff(Pawn pawn)
        {
            //first, we randomly decided if we should even apply the hediff

            //If the random number is not within the chance range, exit.
            if (!Rand.Chance(chance))
            {
                return;
            }
            //If both pawn is male AND the male chance check fails, exit.
            if (pawn.gender == Gender.Male && !Rand.Chance(maleCommonality))
            {
                return;
            }
            //If both pawn is female AND the female chance check fails, exit.
            if (pawn.gender == Gender.Female && !Rand.Chance(femaleCommonality))
            {
                return;
            }

            //for each body part listed in the hediffgiver's xml, named by def
            foreach (BodyPartDef partDef in partsToAffect)
            {
                //gets all actual parts a pawn has, excluding missing parts, which correspond to the current partDef
                IEnumerable<BodyPartRecord> partRecordsToAffect = pawn.health.hediffSet.GetNotMissingParts()
                    .Where(part => part.def == partDef);
                //for each actual part
                foreach (BodyPartRecord partRecord in partRecordsToAffect)
                {
                    //if the hediffgiver affects every part, or if RNG is within the individual part chance
                    //frankly the bool is redundant but it helps XML legibility
                    if (affectsEveryPart == true || Rand.Chance(individualPartChance))
                    {
                        //creates a concrete hediff from the hediffdef named in the hediffgiver xml, which is suited for the pawn at that actual part
                        Hediff addedHediff = HediffMaker.MakeHediff(hediff, pawn, partRecord);
                        //actually adds that hediff to the pawn
                        pawn.health.AddHediff(addedHediff);
                    }
                }
                //at this point, an appropriate hediff has been added to each actual part corresponding to the current partDef
            }
            //at this point, an appropriate hediff has been added to each actual part corresponding to all called-for partDefs
        }
    }

    //this postfix applies our hediffgiver on spawn rather than over time, as hediffgivers are typically used for tracking pawns' deterioration with age
    [HarmonyPatch(typeof(PawnGenerator), "GeneratePawn", new[] { typeof(PawnGenerationRequest) })]
    static class GeneratePawn_StartWithHediff
    {
        [HarmonyPostfix]
        private static void CallSpawnWithHediffGivers(ref Pawn __result)
        {
            try
            {
                //startGivers is a list of any HediffGiver_StartWithHediff among the hediffGivers taken from the pawn's raceprops' hediffGiverSets, which are then cast as their specific hediffgiver subclass
                IEnumerable<HediffGiver_StartWithHediff> startGivers = __result?.def?.race?.hediffGiverSets?
                    .SelectMany(set => set.hediffGivers)
                    .Where(hediffGiver => hediffGiver is HediffGiver_StartWithHediff)
                    .Cast<HediffGiver_StartWithHediff>();

                //if there weren't any hediffgivers of that type, then skip this; we're done here
                if (startGivers.EnumerableNullOrEmpty())
                {
                    return;
                }

                //for each of those hediffgivers, run the code that determines to how many and which of the pawn's parts it should apply to
                foreach (HediffGiver_StartWithHediff giver in startGivers)
                {
                    giver.GiveHediff(__result);
                }
            }
            catch (Exception e)
            {
                Log.Warning("RimWU: Something went wrong when trying to create a pawn with a starting hediff, Error:\n" + e);
            }
        }
    }
}
