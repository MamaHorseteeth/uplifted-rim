﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;

namespace RimWU
{
	//copied from ThoughtWorker_HumanLeatherApparel
	public class ThoughtWorker_AnimalPersonLeatherApparel : ThoughtWorker
	{
		public static ThoughtState CurrentThoughtState(Pawn p)
		{
			string text = null;
			int num = 0;
			List<Apparel> wornApparel = p.apparel.WornApparel;
			for (int i = 0; i < wornApparel.Count; i++)
			{
				//changed to check if the stuff of the worn apparel is among the thingdefs marked "cruel" by a flag
				if (Common.CruelAnimalResourcesList.Contains(wornApparel[i].Stuff))
				{
					if (text == null)
					{
						text = wornApparel[i].def.label;
					}
					num++;
				}
			}
			if (num == 0)
			{
				return ThoughtState.Inactive;
			}
			if (num >= 5)
			{
				return ThoughtState.ActiveAtStage(4, text);
			}
			return ThoughtState.ActiveAtStage(num - 1, text);
		}

		protected override ThoughtState CurrentStateInternal(Pawn p)
		{
			return ThoughtWorker_AnimalPersonLeatherApparel.CurrentThoughtState(p);
		}
	}

	//copied from ThoughtWorker_Precept_HumanLeatherApparel
	public class ThoughtWorker_Precept_AnimalPersonLeatherApparel : ThoughtWorker_Precept
	{
		protected override ThoughtState ShouldHaveThought(Pawn p)
		{
			return ThoughtWorker_AnimalPersonLeatherApparel.CurrentThoughtState(p);
		}
	}
}
