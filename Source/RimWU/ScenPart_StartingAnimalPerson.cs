﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;
using UnityEngine;

namespace RimWU
{
    public class ScenPart_StartingAnimalPerson : ScenPart
    {
        private PawnKindDef animalPersonKind;
        private int count = 1;
        //StartingAnimal would have a bond chance here
        private string countBuf;
		private static readonly List<Pair<int, float>> CompanionCountChances = new List<Pair<int, float>>
		{
			new Pair<int, float>(1, 20f),
			new Pair<int, float>(2, 10f),
			new Pair<int, float>(3, 5f),
			new Pair<int, float>(4, 3f),
			new Pair<int, float>(5, 1f),
			new Pair<int, float>(6, 1f),
			new Pair<int, float>(7, 1f),
			new Pair<int, float>(8, 1f),
			new Pair<int, float>(9, 1f),
			new Pair<int, float>(10, 0.1f),
			new Pair<int, float>(11, 0.1f),
			new Pair<int, float>(12, 0.1f),
			new Pair<int, float>(13, 0.1f),
			new Pair<int, float>(14, 0.1f)
		};
		public const float VeneratedAnimalWeight = 8f;

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look<PawnKindDef>(ref this.animalPersonKind, "animalPersonKind");
            Scribe_Values.Look<int>(ref this.count, "count", 0, false);
			//Scribe for bond chance removed here as animal-people cannot bond like animals do
        }

		public override void DoEditInterface(Listing_ScenEdit listing)
		{
			Rect scenPartRect = listing.GetScenPartRect(this, ScenPart.RowHeight * 2f);
			Listing_Standard listing_Standard = new Listing_Standard();
			listing_Standard.Begin(scenPartRect.TopHalf());
			listing_Standard.ColumnWidth = scenPartRect.width;
			listing_Standard.TextFieldNumeric<int>(ref this.count, ref this.countBuf, 1f, 1E+09f);
			listing_Standard.End();
			if (Widgets.ButtonText(scenPartRect.BottomHalf(), this.CurrentAnimalPersonLabel().CapitalizeFirst(), true, true, true))
			{
				List<FloatMenuOption> list = new List<FloatMenuOption>();
				list.Add(new FloatMenuOption("RandomAnimalPersonCompanion".Translate().CapitalizeFirst(), delegate ()
				{
					this.animalPersonKind = null;
				}, MenuOptionPriority.Default, null, null, 0f, null, null, true, 0));
				foreach (PawnKindDef localKind2 in this.PossibleAnimalPeople(false))
				{
					PawnKindDef localKind = localKind2;
					list.Add(new FloatMenuOption(localKind.LabelCap, delegate ()
					{
						this.animalPersonKind = localKind;
					}, MenuOptionPriority.Default, null, null, 0f, null, null, true, 0));
				}
				Find.WindowStack.Add(new FloatMenu(list));
			}
		}

		private IEnumerable<PawnKindDef> PossibleAnimalPeople(bool checkForTamer = true)
		{
			return from td in DefDatabase<PawnKindDef>.AllDefs
				   where td.HasModExtension<TamedWildManFlag>() //changed from check for .Animal && (!checkForTamer || CanKeepPetTame); reason: keeping animal people tame is not a concern like it is with animals. also the check for tamer bool was making it not work, strangely
				   select td;
		}

		//CanKeepPetTame has not been copied over becase keeping animal people tame is not a mechanic in my mod and will not be

		private IEnumerable<PawnKindDef> RandomCompanions()
		{
			return from td in this.PossibleAnimalPeople(true)
				   where td.RaceProps.petness > 0f
				   select td;
		}

		private string CurrentAnimalPersonLabel()
		{
			if (this.animalPersonKind == null)
			{
				return "RandomAnimalPersonCompanion".TranslateSimple();
			}
			return this.animalPersonKind.label;
		}

		public override string Summary(Scenario scen)
		{
			return ScenSummaryList.SummaryWithList(scen, "PlayerStartsWith", ScenPart_StartingThing_Defined.PlayerStartWithIntro);
		}

		public override IEnumerable<string> GetSummaryListEntries(string tag)
		{
			if (tag == "PlayerStartsWith")
			{
				yield return this.CurrentAnimalPersonLabel().CapitalizeFirst() + " x" + this.count;
			}
			yield break;
		}

		public override void Randomize()
		{
			if (Rand.Value < 0.5f)
			{
				this.animalPersonKind = null;
			}
			else
			{
				this.animalPersonKind = this.PossibleAnimalPeople(false).RandomElement<PawnKindDef>();
			}
			this.count = ScenPart_StartingAnimalPerson.CompanionCountChances.RandomElementByWeight((Pair<int, float> pa) => pa.Second).First;
			//bond to random player character count removed as bonds between animal people and regular people are known as love
		}

		public override bool TryMerge(ScenPart other)
		{
			ScenPart_StartingAnimalPerson scenPart_StartingAnimalPerson = other as ScenPart_StartingAnimalPerson;
			if (scenPart_StartingAnimalPerson != null && scenPart_StartingAnimalPerson.animalPersonKind == this.animalPersonKind)
			{
				this.count += scenPart_StartingAnimalPerson.count;
				return true;
			}
			return false;
		}

		private float CompanionWeight(PawnKindDef animalPerson)
		{
			FactionIdeosTracker ideos = Find.GameInitData.playerFaction.ideos;
			Ideo ideo = (ideos != null) ? ideos.PrimaryIdeo : null;
			if (ideo != null)
			{
				using (List<ThingDef>.Enumerator enumerator = ideo.VeneratedAnimals.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (enumerator.Current == animalPerson.race)
						{
							return 8f;
						}
					}
				}
			}
			return animalPerson.RaceProps.petness;
		}

		public override IEnumerable<Thing> PlayerStartingThings()
		{
			int num;
			for (int i = 0; i < this.count; i = num + 1)
			{
				PawnKindDef kindDef;
				if (this.animalPersonKind != null)
				{
					kindDef = this.animalPersonKind;
				}
				else
				{
					kindDef = this.RandomCompanions().RandomElementByWeight((PawnKindDef td) => this.CompanionWeight(td));
				}
				Pawn animalPerson = PawnGenerator.GeneratePawn(kindDef, Faction.OfPlayer);
				if (animalPerson.Name == null || animalPerson.Name.Numerical)
				{
					animalPerson.Name = PawnBioAndNameGenerator.GeneratePawnName(animalPerson, NameStyle.Full, null);
				}
				//an entire if statement for assigning a bonded pet to a master has been removed here, as animal people bond through conventional love
				yield return animalPerson;
				num = i;
			}
			yield break;
		}
	}
}
