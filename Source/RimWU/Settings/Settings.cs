﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RimWorld;
using Verse;
using UnityEngine;
using HugsLib;
using HugsLib.Settings;

namespace RimWU
{
    public class Settings : ModSettings
    {
        public bool extendRimWUBehaviorToVanillaWildmen = false;
        
        public void Reset()
        {

        }
        
        public void DefsLoaded()
        {

        }
        
        public override void ExposeData()
        {
            
            if (Scribe.mode == LoadSaveMode.Saving)
            {
                
            }

            Scribe_Values.Look(ref extendRimWUBehaviorToVanillaWildmen, "extendRimWUBehaviorToVanillaWildmen");
            base.ExposeData();

            if (Scribe.mode == LoadSaveMode.LoadingVars)
            {

            }
        }
    }

public class RimWildlifeUplifted : Mod
    {
        public static Settings settings;
        public static RimWildlifeUplifted mod;

        public RimWildlifeUplifted(ModContentPack content) : base(content)
        {
            settings = GetSettings<Settings>();
            mod = this;
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);

            listingStandard.CheckboxLabeled("RimWU_ExtendRimWUBehaviorToVanillaWildmen_Label".Translate(), ref settings.extendRimWUBehaviorToVanillaWildmen, "RimWU_ExtendRimWUBehaviorToVanillaWildmen_ToolTip".Translate());
            


            listingStandard.End();
            base.DoSettingsWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "RimWULong".Translate();
        }
    }

    public static class CommonSettings
    {
        public static bool extendRimWUBehaviorToVanillaWildmen = LoadedModManager.GetMod<RimWildlifeUplifted>().GetSettings<Settings>().extendRimWUBehaviorToVanillaWildmen;
    }

    //made using hugslib
    /*public class HugsLibSettings : ModBase
    {
        public override string ModIdentifier
        {
            get { return "RimWildlifeUplifted"; }
        }
        private SettingHandle<bool> toggle;
        private SettingHandle<int> spinner;

        private enum HandleEnum { DefaultValue, ValueOne, ValueTwo }

        public override void DefsLoaded()
        {
            toggle = Settings.GetHandle<bool>(
                "myToggle", //identifier
                "toggleSetting_title".Translate(), //label. can be .Translate() 
                "toggleSetting_desc".Translate(), //tooltip. can be .Translate(). if null, no tooltip will be displayed
                false); //default value. if the user doesn't modify this setting, it will not be stored, so changes to the default value will affect a user's experience

            spinner = Settings.GetHandle(
                "intSpinner",
                "spinnerSetting_title".Translate(),
                "spinnerSetting_desc".Translate(),
                0,
                Validators.IntRangeValidator(0, 30)); //ensures that the only acceptable values are between 0 and 30 (inclusive) 

            var enumHandle = Settings.GetHandle(
                "enumThing", //identifier
                "enumSetting_title".Translate(), //label
                "enumSetting_desc".Translate(), //tooltip
                HandleEnum.DefaultValue, //default value
                null, //validator; not required but because it's the fifth parameter, it needs to be listed as null to enter in the enumSetting translations
                "enumSetting_"); //this needs actual translations in the language files for each enum option

        }

        
    }*/
}
